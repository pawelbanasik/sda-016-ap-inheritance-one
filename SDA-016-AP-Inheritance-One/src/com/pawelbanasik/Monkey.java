package com.pawelbanasik;

public class Monkey extends Animal {

	public Monkey(boolean isPet) {
		super(isPet);
	}

	@Override
	public void giveSound() {
		System.out.println("Monkey sound...");
	}

	@Override
	public int getLegsAmount() {
		return 2;
	}
}
