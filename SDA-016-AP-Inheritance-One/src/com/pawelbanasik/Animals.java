package com.pawelbanasik;

public class Animals {

	public static void main(String[] args) {

		Cat cat1 = new Cat();
		Dog dog1 = new Dog();
		Hamster hamster1 = new Hamster();
		Monkey monkey1 = new Monkey(true);

		// 1 sposob
		cat1.giveSound();
		dog1.giveSound();
		hamster1.giveSound();
		monkey1.giveSound();

		// 2 sposob
		Animal[] zwierzeta = new Animal[] { cat1, dog1, hamster1, monkey1 };

		for (Animal i : zwierzeta) {
			i.giveSound();
		}

		// 3 sposob
		Animal[] animals = new Animal[] {

				new Cat(), new Dog(), new Hamster(), new Monkey(true), new Monkey(false)

		};
		for (Animal i : animals) {
			i.giveSound();
			System.out.println("Mam " + i.getLegsAmount() + " nogi.");

		}

		animals[2].giveSound();

		for (Animal animal : animals) {

			animal.giveSound();
			if (animal.isPet()) {

				System.out.println("Jestem zwierzeciem domowym");
			} else {

				System.out.println("Jestem zwierzeciem dzikim");
			}
		}

	}
}
