package com.pawelbanasik;

public class Hamster extends Animal {

	public Hamster() {
		super(true);
	}

	@Override
	public void giveSound() {
		System.out.println("Hamster sound...");
	}
}
