package com.pawelbanasik;

public class Dog extends Animal {
	public Dog() {
		super(true);
	}

	@Override
	public void giveSound() {
		System.out.println("Hau, Hau!");
	}
}
